import React, { Component } from 'react';
import './JobVacancy.css';

export class JobVacancy extends Component {
    static displayName = JobVacancy.name;

    constructor(props) {
        super(props);
        this.state = { jobs: [], loading: true, name: "" };
    }

    componentDidMount() {
        this.populateData("");
    }

    badgeCode(strTitle) {
        var matches = strTitle.match(/\b(\w)/g);
        return matches.join('');
    }

    formatCurrency(num) {
        return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }
    formatDate(strDate) {
        return new Date(strDate).toLocaleDateString();
    }

    /**
     * Set the search text data into the state object
     * @param event
     */
    searchByText = (event) => {
        const keyword = event.target.value;

        if (keyword.length > 2 || keyword.length === 0) {
            this.setState({ loading: true });
            this.populateData(keyword);
        } else {
            this.setState({ name: keyword });
        }
    }

    renderDataTable(jobs) {
        return (
            <div className="cards">
                {(jobs.map(job =>
                    <div className="job-card" key={job.id}>
                        <div className="row">
                            <div className="card-header-column">
                                <span className="badge">
                                    {this.badgeCode(job.title)}
                                </span>
                            </div>
                            <div className="card-header-column">
                                <div className="text-header-type">
                                    {job.jobType}
                                </div>
                                <div className="text-header">
                                    {job.title}
                                </div>
                                <div className="text-date-header">
                                    <span className="text-title-sm"> Date Created:</span>
                                    {this.formatDate(job.dateTimeAdded)}
                                </div>
                            </div>
                        </div>
                        <div className="card-salary">
                            <div className="text-salary-header">
                                <span className="text-title-sm"> Salary:</span>
                                {this.formatCurrency(job.salary)}
                            </div>
                        </div>
                        <div className="card-desc">
                            {job.description}
                        </div>
                        <div className="card-row">
                            <a className="default_btn">Apply</a>
                        </div>
                    </div>))}
            </div>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderDataTable(this.state.jobs);

        return (
            <div>
                <h2 id="Label">Jobs</h2>
                <div className="form-group">

                    <div className="group">
                        <input type="search" id="name" name="name" value={this.state.name} onChange={(e) => this.searchByText(e)} />
                        <span className="highlight"></span>
                        <span className="bar"></span>
                        <label>Search</label>
                    </div>
                </div>
                <div className="jobs-container"> {contents} </div>
            </div>
        );
    }

    async populateData(searchParam) {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(searchParam)
        };
        console.log(requestOptions);

        const response = await fetch('api/job/search', requestOptions);
        const data = await response.json();

        this.setState({ jobs: data, loading: false, name: searchParam });
    }

}
