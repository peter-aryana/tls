﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TLS.DataContracts;
using TLS.Web.BizServices;

namespace TLS.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class JobController : ControllerBase
    {
        private readonly JobBizService _jobService;

        public JobController(JobBizService service)
        {
            _jobService = service;
        }

        [HttpGet("All")]
        public async Task<IEnumerable<JobViewModel>> GetAllAsync()
        {
            return await _jobService.GetAllAsync();
        }

        [HttpPost("Search")]
        public async Task<IEnumerable<JobViewModel>> SearchAsync([FromBody]string keyword)
        {
            return await _jobService.SearchAsync(keyword);
        }
    }
}
