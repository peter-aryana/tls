﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TLS.DataContracts;
using TLS.DataRepository;

namespace TLS.Web.BizServices
{
    public class JobBizService
    {
        private readonly IMapper _mapper;
        private readonly JobDataRepository _jobDataRepository;

        public JobBizService(IMapper mapper, JobDataRepository jobDataRepository)
        {
            _mapper = mapper;
            _jobDataRepository = jobDataRepository;
        }

        public async Task<IEnumerable<JobViewModel>> GetAllAsync()
        {
            var results = await _jobDataRepository.GetAllAsync();
            return results.Select(x=> _mapper.Map<JobViewModel>(x));
        }

        public async Task<IEnumerable<JobViewModel>> SearchAsync(string searchTitle)
        {
            // return all results when no search parameter is provided.
            if (string.IsNullOrWhiteSpace(searchTitle))
            {
                return await GetAllAsync();
            }

            var results = await _jobDataRepository.SearchAsync(searchTitle);
            return results.Select(x => _mapper.Map<JobViewModel>(x));
        }
    }
}
