﻿using System.Collections.Generic;
using TLS.DataContracts;
using TLS.DataRepository.Models;

namespace TLS.DataMapper
{
    public class ModelMappings : AutoMapper.Profile
    {
        public ModelMappings()
        {
            CreateMap<Job, JobViewModel>().ReverseMap();
            CreateMap<List<Job>, List<JobViewModel>>().ReverseMap();
        }
    }
}
