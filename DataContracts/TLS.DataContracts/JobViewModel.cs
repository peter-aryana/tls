﻿using System;

namespace TLS.DataContracts
{
    public class JobViewModel
    {
        public Guid Id { get; set; }
        public string JobType { get; set; }
        public decimal Salary { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTimeOffset DateTimeAdded { get; set; }
    }
}
