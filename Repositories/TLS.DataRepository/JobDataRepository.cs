﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TLS.DataRepository.Models;

namespace TLS.DataRepository
{
    public class JobDataRepository
    {
        private readonly DataCoreContext _context;

        public JobDataRepository(DataCoreContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Job>> GetAllAsync()
        {
            return await _context.Jobs
                    .OrderByDescending(x => x.DateTimeAdded)
                    .ToListAsync();
        }

        public async Task<IEnumerable<Job>> SearchAsync(string searchTitle)
        {
            //nothing provided don't search.
            if (string.IsNullOrWhiteSpace(searchTitle))
            {
                return Enumerable.Empty<Job>();
            }

            return await _context.Jobs
                    .Where(x=> x.Title.ToLower().Contains(searchTitle.ToLower()))
                    .OrderByDescending(x=>x.DateTimeAdded)
                    .ToListAsync();
        }
    }
}
