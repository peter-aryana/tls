﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TLS.DataRepository.Models
{
    [Table("Job")]
    public partial class Job
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }
        [Required]
        [Column("jobType")]
        [StringLength(50)]
        public string JobType { get; set; }
        [Column("salary", TypeName = "decimal(18, 2)")]
        public decimal Salary { get; set; }
        [Required]
        [Column("title")]
        [StringLength(250)]
        public string Title { get; set; }
        [Required]
        [Column("description")]
        public string Description { get; set; }
        [Column("dateTimeAdded")]
        public DateTimeOffset DateTimeAdded { get; set; }
    }
}